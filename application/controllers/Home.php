<?php
class Home extends CI_Controller
{
	function index(){
			$data['title'] = "Toko Bangunan";
			$data['content'] = $this->db->get('tbl_website')->row();
			$data['kategori'] = $this->db->get('tbl_kategori')->result();
			$q = $this->db->get_where('tbl_customers', array('nomor_hp' => $this->session->userdata('hp')))->row('id');
			$data['cart'] = $this->db->get_where('tbl_cart', array('id_customer' => $q))->num_rows();
			$data['isi'] = $this->load->view('front/content',[],true);
			$this->load->view('front/index',$data);
		}
	function login(){
		$hp = $this->input->post('hp');
		$password = md5($this->input->post('password'));
		$q = $this->db->get_where('tbl_customers', array('nomor_hp' => $hp, 'password' => $password));
		echo $q->num_rows();
		if ($q->num_rows == 1) {
			# code...
			$qs = $q->row();
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Sukses!", "Login Berhasil", "success");
      		</script>');
      		$this->session->set_userdata('nama', $qs->nama_panggilan);
      		$this->session->set_userdata('hp', $hp);
      		$this->session->set_userdata('status', 'login_user');
      		redirect(base_url());
		}else{
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Gagal!", "Nomor HP / Password Salah", "error");
      		</script>');
      		redirect(base_url());
		}
	}
	function logout(){
		session_destroy();
		redirect(base_url());
	}
	function register(){
		$nama_lengkap = $this->input->post('fullname');
		$nama = $this->input->post('name');
		$hp = $this->input->post('hp');
		$telp = $this->input->post('telp');
		$alamat = $this->input->post('alamat');
		$rt = $this->input->post('rt');
		$rw = $this->input->post('rw');
		$password = md5($this->input->post('password'));

		$q = $this->db->get_where('tbl_customers', array('nomor_hp' => $hp))->num_rows();
		if ($q > 1) {
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Gagal!", "Nomor HP telah terdaftar", "error");
      		</script>');
      		redirect(base_url());
		}else{
			$data = array(
							'nama_lengkap' => $nama_lengkap,
							'nama_panggilan' => $nama,
							'nomor_hp' => $hp,
							'nomor_telp' => $telp,
							'alamat' => $alamat,
							'rt' => $rt,
							'rw' => $rw,
							'password' => $password
						 );
			$this->db->insert('tbl_customers', $data);
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Sukses!", "Pendaftaran Berhasil", "success");
      		</script>');
      		$this->session->set_userdata('nama', $nama);
      		$this->session->set_userdata('hp', $hp);
      		$this->session->set_userdata('status', 'login_user');
      		redirect(base_url());
		}
	}
}
?>