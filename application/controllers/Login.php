<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
    public function index(){
        $this->load->view('login/login');
    }

    public function login_act(){
		$username = $this->input->post('username');
		$pass = md5($this->input->post('password'));
		$where = array(
						'username' => $username,
						'password' => $pass
					   );
		$q = $this->db->get_where('tbl_user', $where);
		echo $q->num_rows();
		if ($q->num_rows() == 1) {
			# code...
			$qs = $q->row();
			// echo $qs->full_name;
			$data = array(
							'fullname' => $qs->fullname,
							'username' => $qs->username,
							'level' => $qs->level,
							'login' => 'login_admin'
						 );
			$this->session->set_userdata($data);
			redirect('dashboard');
		}else{
			$this->session->set_userdata('gagal_login', '<script type="text/javascript">
        swal("Oops!", "username/password salah!", "error");
      </script>');
			redirect('dashboard');
		}
	}
	function logout(){
		session_destroy();
		redirect(base_url());
	}
}