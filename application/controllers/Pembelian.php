<?php 
/**
* 
*/
class Pembelian extends CI_Controller
{
	
	function __construct()
	{
				parent::__construct();
				if($this->session->userdata('login') != 'login_admin')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
	}
	function index(){
		$data['title'] = "Toko Bangunan";
        $data['menu'] = $this->Login_m->menu();
        $id_user = $this->db->get_where('tbl_user', array('username' => $this->session->userdata('username')))->row()->id;
        $content['bill'] = $this->db->query('SELECT tbl_cart.*, tbl_product_list.* FROM tbl_cart INNER JOIN tbl_product_list ON tbl_cart.id_product = tbl_product_list.id WHERE tbl_cart.id_user = '.$id_user.' AND tbl_cart.is_bayar = 0')->result();
        $content['produk'] = $this->db->query('SELECT tbl_product_list.*, tbl_kategori.kategori_barang FROM tbl_product_list INNER JOIN tbl_kategori ON tbl_product_list.kategori = tbl_kategori.id_kategori')->result();
		$data['content'] = $this->load->view('dashboard/daftar_produk',$content,true);
		$this->load->view('dashboard/index',$data);
    }
    function hapus_bill($id){
        $get = $this->db->get_where('tbl_cart', array('id_cart' => $id))->row();
        $qty = $get->qty;
        $id_product = $get->id_product;
        $q = $this->db->get_where('tbl_product_list', array('id' => $id_product))->row()->jumlah_barang;
        $total_qty = $qty + $q;
        $this->db->update('tbl_product_list', array('jumlah_barang' => $total_qty), array('id' => $id_product));
        $this->db->delete('tbl_cart', array('id_cart' => $id));
        redirect('pembelian');
    }
    function add(){
        $id_produk = $this->input->post('id_produk');
        $qty = $this->input->post('qty');
        $id_user = $this->db->get_where('tbl_user', array('username' => $this->session->userdata('username')))->row()->id;

        // echo $qty + 1;
        $data = array(
            'id_user' => $id_user,
            'id_product' => $id_produk,
            'qty' => $qty,
            'is_bayar' => 0
        );
        $get_product = $this->db->get_where('tbl_product_list', array('id' => $id_produk))->row()->jumlah_barang;
        $update_jumlah_barang = $get_product - $qty;

        $this->db->update('tbl_product_list', array('jumlah_barang' => $update_jumlah_barang), array('id' => $id_produk));
        $this->db->insert('tbl_cart', $data);
        redirect('pembelian');
    }
    function pembayaran(){
        $total = $this->input->post('total');
        $data_id = $this->input->post('data_id');
        $nama = $this->input->post('nama');
        $hp = $this->input->post('hp');
        $alamat = $this->input->post('alamat');
        $status = $this->input->post('kategori');
        $harga = $this->input->post('harga');
        $sisa_piutang = $this->input->post('piutang');
        $piutang = $total - $harga;

        $data = array(
                        'nama_pembeli' => $nama,
                        'hp' => $hp,
                        'alamat' => $alamat,
                        'status_pembayaran' => $status,
                        'terbayar' => $harga,
                        'sisa_piutang' => $piutang,
                        'total' => $total,
                        'produk_dibeli' => $data_id
                     );
        $id_cart_cek = explode('||', $data_id);
        $id = count($id_cart_cek) - 1;
        for ($i=0; $i < $id; $i++) { 
            # code...

            $this->db->update('tbl_cart', array('is_bayar' => 1), array('id_cart' => $id_cart_cek[$i]));
        }
        $this->db->insert('detail_cart', $data);
        $this->session->set_userdata('notif', '<script type="text/javascript">
            swal("Sukses!", "Pembayaran Berhasil", "success");
              </script>');
            redirect('pembelian');
    }
}
?>