<?php
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Transaksi extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('status') != 'login')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
			}
	function index(){
			$data['title'] = "Dashboard";
			$data['menu'] = $this->Login_m->menu();		
			$data['content'] = $this->load->view('dashboard/transaksi_index',[],true);
			$this->load->view('dashboard/index',$data);
		}
	function setor(){
		$this->session->set_userdata('transaksi', 'setor');
		$data['title'] = "Nasabah Setor";
		$data['menu'] = $this->Login_m->menu();
		if ($this->session->userdata('level') != 2) {
			# code...
			$setor['data'] = $this->db->query('SELECT tbl_user.*, tbl_setor.*, tbl_jenis.jenis_sampah as nama_sampah, tbl_jenis.satuan from tbl_user INNER JOIN tbl_setor ON tbl_user.username = tbl_setor.username INNER JOIN tbl_jenis ON tbl_setor.jenis_sampah = tbl_jenis.id where tbl_user.level = 2 ORDER BY tbl_setor.id DESC')->result();
		}else{
			$setor['data'] = $this->db->query('SELECT tbl_user.*, tbl_setor.*, tbl_jenis.jenis_sampah as nama_sampah, tbl_jenis.satuan from tbl_user INNER JOIN tbl_setor ON tbl_user.username = tbl_setor.username INNER JOIN tbl_jenis ON tbl_setor.jenis_sampah = tbl_jenis.id where tbl_user.username = "'.$this->session->userdata('username').'" ORDER BY tbl_setor.id DESC')->result();
		}
		$data['content'] = $this->load->view('dashboard/setor_list',$setor,true);
		$this->load->view('dashboard/index',$data);
	}
	function nasabah(){
		$data['title'] = "Nasabah";
		$data['menu'] = $this->Login_m->menu();
		$nasabah['data'] = $this->db->query('SELECT * FROM tbl_user where level = 2')->result();
		$data['content'] = $this->load->view('dashboard/nasabah',$nasabah,true);
		$this->load->view('dashboard/index',$data);
	}
	function input_setor($username=''){
		if ($username == '') {
			# code...
			redirect('transaksi/nasabah');
		}else{
			$data['title'] = "Nasabah Setor";
			$data['menu'] = $this->Login_m->menu();
			$setor['username'] = $username; 
			$setor['fullname'] = $this->db->get_where('tbl_user', array('username' => $username))->row();
			$setor['jenis_sampah'] = $this->db->get('tbl_jenis')->result();
			$data['content'] = $this->load->view('dashboard/setor',$setor,true);
			$this->load->view('dashboard/index',$data);
		}
	}
	function setor_simpan(){
		$n = count($this->input->post('username'));
		$username = $this->input->post('user');
		$total_harga = 0;
		$jumlah_harga = 0;
		$saldo_sekarang = 0;
		$data_ = array();
		for ($i=0; $i < $n; $i++) { 
			# code...
			$jenis = $this->input->post('jenis'.$i);
			$berat = $this->input->post('berat'.$i);

			$q = $this->db->get_where('tbl_jenis', array('id' => $jenis))->row();
			$total_harga = $q->harga_sampah * $berat;
			
			$data = array(
							'username' => $username,
							'jenis_sampah' => $jenis,
							'berat_sampah' => $berat,
							'total_harga' => $total_harga
						 );
			 array_push($data_, array(
										'jenis_sampah' => $q->jenis_sampah,
										'satuan' => $q->satuan,
										'harga_sampah' => $q->harga_sampah,
										'berat_sampah' => $berat,
										'total_harga' => $total_harga
						 			));
			$this->db->insert('tbl_setor', $data);
			$jumlah_harga += $total_harga;
		}
		$get_saldo = $this->db->get_where('tbl_saldo', array('username' => $username))->row()->total_saldo;
		$saldo_sekarang = $jumlah_harga + $get_saldo;
		$this->db->update('tbl_saldo', array('total_saldo' => $saldo_sekarang), array('username' => $username));
		$data_session = array(
								'data' => $data_,
								'total' => $jumlah_harga,
								'saldo' => $saldo_sekarang
							  );
		$this->session->set_userdata($data_session);
		$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Setoran Nasabah Berhasil di Simpan", "success");
      		</script>');
		redirect('transaksi/input_setor/'.$username);
	}
	function tarik($username=''){
		if ($username == '') {
			# code...
			redirect('transaksi/nasabah');
		}else{
			$data['title'] = "Nasabah Tarik";
			$data['menu'] = $this->Login_m->menu();
			$tarik['username'] = $username; 
			$tarik['data'] = $this->db->query('SELECT tbl_user.*, tbl_saldo.* FROM tbl_user INNER JOIN tbl_saldo ON tbl_user.username = tbl_saldo.username WHERE tbl_user.username = "'.$username.'"')->result();
			$tarik['jenis_sampah'] = $this->db->get('tbl_jenis')->result();
			$data['content'] = $this->load->view('dashboard/tarik',$tarik,true);
			$this->load->view('dashboard/index',$data);
		}
	}
	function tarik_act(){
		$username = $this->input->post('username');
		$tarik = $this->input->post('tarik');
		$saldo_sekarang = 0;
		$get_saldo = $this->db->get_where('tbl_saldo', array('username' => $username))->row()->total_saldo;
		$saldo_sekarang = $get_saldo - $tarik;
		if ($saldo_sekarang < 1) {
			# code...
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Maaf!", "Saldo yang dimiliki tidak mencukupi", "error");
      		</script>');
			redirect('transaksi/tarik/'.$username);
		}else{
			$this->db->update('tbl_saldo', array('total_saldo' => $saldo_sekarang), array('username' => $username));
			$this->db->insert('tbl_transaksi', array('username' => $username, 'total_penarikan' => $tarik));
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Berhasil menarik tunai", "success");
      		</script>');
      		redirect('transaksi/tarik/'.$username);
		}

	}
	function tarik_tunai(){
		$this->session->set_userdata('transaksi', 'tarik_tunai');
		$data['title'] = "Tarik Tunai";
		$data['menu'] = $this->Login_m->menu();
		if ($this->session->userdata('level') != 2) {
			$Tarik['data'] = $this->db->query('SELECT tbl_user.*, tbl_transaksi.*, tbl_saldo.* from tbl_user INNER JOIN tbl_transaksi ON tbl_user.username = tbl_transaksi.username INNER JOIN tbl_saldo ON tbl_user.username = tbl_saldo.username where tbl_user.level = 2 ORDER BY tbl_transaksi.id DESC')->result();
		}else{
			$Tarik['data'] = $this->db->query('SELECT tbl_user.*, tbl_transaksi.*, tbl_saldo.* from tbl_user INNER JOIN tbl_transaksi ON tbl_user.username = tbl_transaksi.username INNER JOIN tbl_saldo ON tbl_user.username = tbl_saldo.username where tbl_user.username = "'.$this->session->userdata('username').'" ORDER BY tbl_transaksi.id DESC')->result();
		}
		$data['content'] = $this->load->view('dashboard/list_tarik',$Tarik,true);
		$this->load->view('dashboard/index',$data);
	}
    function export_setor(){
    // Load plugin PHPExcel nya
    include APPPATH.'third_party/PHPExcel/PHPExcel.php';
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Bank Sampah')
                 ->setLastModifiedBy('Bank Sampah')
                 ->setTitle("Data Setor Nasabah")
                 ->setSubject("Setoran")
                 ->setDescription("Laporan Semua Data Setoran Nasabah")
                 ->setKeywords("Data Setoran Nasabah");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA SETORAN NASABAH"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "NAMA LENGKAP"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "JENIS SAMPAH"); // Set kolom C3 dengan tulisan "NAMA"
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "BERAT SAMPAH"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "TOTAL HARGA"); // Set kolom E3 dengan tulisan "ALAMAT"
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "LOG"); // Set kolom E3 dengan tulisan "ALAMAT"
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    
    $setor = $this->db->query('SELECT tbl_user.*, tbl_setor.*, tbl_jenis.jenis_sampah as nama_sampah, tbl_jenis.satuan from tbl_user INNER JOIN tbl_setor ON tbl_user.username = tbl_setor.username INNER JOIN tbl_jenis ON tbl_setor.jenis_sampah = tbl_jenis.id where tbl_user.level = 2 ORDER BY tbl_setor.id DESC')->result();
    $no = 1; 
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($setor as $data){ 
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->fullname);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama_sampah);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->berat_sampah." ".$data->satuan);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, number_format($data->total_harga,2,',','.'));
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->log);
      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(50); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom E
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Setoran Nasabah");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Setoran Nasabah.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
  }

    //  function export_tarik()
    // {
    //       $semua_data = $this->db->query('SELECT tbl_user.*, tbl_transaksi.*, tbl_saldo.* from tbl_user INNER JOIN tbl_transaksi ON tbl_user.username = tbl_transaksi.username INNER JOIN tbl_saldo ON tbl_user.username = tbl_saldo.username where tbl_user.level = 2 ORDER BY tbl_transaksi.id DESC')->result();

    //       $spreadsheet = new Spreadsheet;

    //       $spreadsheet->setActiveSheetIndex(0)
    //                   ->setCellValue('A1', 'Nama Lengkap')
    //                   ->setCellValue('B1', 'Saldo Seblumnya')
    //                   ->setCellValue('C1', 'Jumlah Penarikan')
    //                   ->setCellValue('D1', 'Saldo Sekarang')
    //                   ->setCellValue('E1', 'Tanggal');

    //       $kolom = 2;
    //       $saldo_sebelumnya = 0;
    //       foreach($semua_data as $p) {
    //       		$saldo_sebelumnya = $p->total_saldo + $p->total_penarikan;
    //            $spreadsheet->setActiveSheetIndex(0)
    //                        ->setCellValue('A' . $kolom, $p->fullname)
    //                        ->setCellValue('B' . $kolom, $saldo_sebelumnya)
    //                        ->setCellValue('C' . $kolom, $p->total_penarikan)
    //                        ->setCellValue('D' . $kolom, $p->total_saldo)
    //                        ->setCellValue('E' . $kolom, $p->waktu_penarikan);

    //            $kolom++;

    //       }

    //       $writer = new Xlsx($spreadsheet);

    //     header('Content-Type: application/vnd.ms-excel');
    //     header('Content-Disposition: attachment;filename="Data Penarikan.xls"');
    //     header('Cache-Control: max-age=0');

    //   $writer->save('php://output');
    //  }
    function export_tarik(){
    // Load plugin PHPExcel nya
    include APPPATH.'third_party/PHPExcel/PHPExcel.php';
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Bank Sampah')
                 ->setLastModifiedBy('Bank Sampah')
                 ->setTitle("Data Penarikan Nasabah")
                 ->setSubject("Penarikan")
                 ->setDescription("Laporan Semua Data Penarikan Nasabah")
                 ->setKeywords("Data Penarikan Nasabah");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA Penarikan NASABAH"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "NAMA LENGKAP"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "SALDO SEBELUMNYA"); // Set kolom C3 dengan tulisan "NAMA"
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "JUMLAH PENARIKAN"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "SALDO SEKARANG"); // Set kolom E3 dengan tulisan "ALAMAT"
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "WAKTU PENARIKAN"); // Set kolom E3 dengan tulisan "ALAMAT"
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    
    $tarik = $this->db->query('SELECT tbl_user.*, tbl_transaksi.*, tbl_saldo.* from tbl_user INNER JOIN tbl_transaksi ON tbl_user.username = tbl_transaksi.username INNER JOIN tbl_saldo ON tbl_user.username = tbl_saldo.username where tbl_user.level = 2 ORDER BY tbl_transaksi.id DESC')->result();
    $no = 1; 
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    $saldo_sebelumnya = 0;
    foreach($tarik as $data){ 
      $saldo_sebelumnya = $data->total_saldo + $data->total_penarikan;
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->fullname);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, number_format($saldo_sebelumnya,2,',','.'));
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, number_format($data->total_penarikan,2,',','.'));
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, number_format($data->total_saldo,2,',','.'));
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->waktu_penarikan);
      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(50); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom E
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Penarikan Nasabah");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Penarikan Nasabah.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
  }
}
?>