<?php 
/**
* 
*/
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Pegawai extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('status') != 'login')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
			}
	function index(){
		$data['title'] = "Pegawai";
		$data['menu'] = $this->Login_m->menu();
		$nasabah['data'] = $this->db->query('SELECT * FROM tbl_user where level <> 2')->result();
		$data['content'] = $this->load->view('dashboard/pegawai',$nasabah,true);
		$this->load->view('dashboard/index',$data);
	}
	function detail($username=''){
		if ($username == '') {
			# code...
			redirect('pegawai');
		}else{
			$data['title'] = "Detail Pegawai";
			$data['menu'] = $this->Login_m->menu();
			$nasabah['data'] = $this->db->get_where('tbl_user', array('username' => $username))->result();
			$data['content'] = $this->load->view('dashboard/edit_pegawai',$nasabah,true);
			$this->load->view('dashboard/index',$data);
		}
	}
	function update_data(){
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$telp = $this->input->post('telp');
		$alamat = $this->input->post('alamat');
		$level = $this->input->post('level');

		$data = array(
						'fullname' => $fullname,
						'email' => $email,
						'hp' => $hp,
						'telp' => $telp,
						'alamat' => $alamat,
						'level' => $level
					 );
		$where = array('id' => $this->input->post('id'));
		$this->db->update('tbl_user', $data, $where);
		$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Data Pegawai Berhasil diubah", "success");
      		</script>');
		redirect('pegawai');
	}
	// function export()
 //    {
 //          $semua_data = $this->db->query('SELECT * FROM tbl_user where level <> 2')->result();

 //          $spreadsheet = new Spreadsheet;

 //          $spreadsheet->setActiveSheetIndex(0)
 //                      ->setCellValue('A1', 'Username')
 //                      ->setCellValue('B1', 'Nama Lengkap')
 //                      ->setCellValue('C1', 'Email')
 //                      ->setCellValue('D1', 'Nomor HP')
 //                      ->setCellValue('E1', 'Nomor Telp')
 //                      ->setCellValue('F1', 'Alamat')
 //                      ->setCellValue('G1', 'Status');

 //          $kolom = 2;
 //          $status = '';
 //          foreach($semua_data as $p) {
 //          		if ($p->level == 0) {
 //          			# code...
 //          			$status = 'Ketua';
 //          		}else{
 //          			$status = 'Pegawai'; 
 //          		}
 //               $spreadsheet->setActiveSheetIndex(0)
 //                           ->setCellValue('A' . $kolom, $p->username)
 //                           ->setCellValue('B' . $kolom, $p->fullname)
 //                           ->setCellValue('C' . $kolom, $p->email)
 //                           ->setCellValue('D' . $kolom, $p->hp)
 //                           ->setCellValue('E' . $kolom, $p->telp)
 //                           ->setCellValue('F' . $kolom, $p->alamat)
 //                           ->setCellValue('G' . $kolom, $status);

 //               $kolom++;

 //          }

 //          $writer = new Xlsx($spreadsheet);

 //        header('Content-Type: application/vnd.ms-excel');
 //        header('Content-Disposition: attachment;filename="Data Pegawai.xls"');
 //        header('Cache-Control: max-age=0');

 //      $writer->save('php://output');
 //     }
	function export(){
    // Load plugin PHPExcel nya
    include APPPATH.'third_party/PHPExcel/PHPExcel.php';
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Bank Sampah')
                 ->setLastModifiedBy('Bank Sampah')
                 ->setTitle("Data Pegawai")
                 ->setSubject("Pegawai")
                 ->setDescription("Laporan Semua Data Pegawai")
                 ->setKeywords("Data Pegawai");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA PEGAWAI"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:H1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "USERNAME"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "NAMA LENGKAP"); // Set kolom C3 dengan tulisan "NAMA"
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "EMAIL"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "NOMOR HP"); // Set kolom E3 dengan tulisan "ALAMAT"
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "NOMOR TELEPON"); // Set kolom E3 dengan tulisan "ALAMAT"
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "ALAMAT LENGKAP"); // Set kolom E3 dengan tulisan "ALAMAT"
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "STATUS"); // Set kolom E3 dengan tulisan "ALAMAT"
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    
    $pegawai = $this->db->query('SELECT * FROM tbl_user where level <> 2')->result();
    $no = 1; 
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    $status = '';
    foreach($pegawai as $data){ 
    	if ($data->level == 0) {
          			# code...
          			$status = 'Ketua';
          		}else{
          			$status = 'Pegawai'; 
          		}
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->username);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->fullname);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->email);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->hp);
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->telp);
      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->alamat);
      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $status);
      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(55); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(50); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15); // Set width kolom E
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Pegawai");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Pegawai.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
  }

}
?>