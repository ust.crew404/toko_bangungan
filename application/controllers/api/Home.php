<?php

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Home extends CI_Controller
{
	 function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
	function get_login(){
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$q = $this->db->get_where('tbl_user', array('username' => $username, 'password' => $password));
		$out = array('error' => false);
		if ($q->num_rows() > 0) {
			# code...
			$qs = $q->row();
			$out['username'] = $qs->result();
			$out['message'] = 'login_sukses';
		}else{
			$out['message'] = 'login_gagal';
		}
		// header("Content-type: application/json");
		// echo json_encode($out);
		$this->response($out, 200);
	}
	// function login_user(){
	// 	$username = $this->input->post('username');
	// 	$password = md5($this->input->post('password'));
	// 	$q = $this->db->get_where('tbl_user', array('username' => $username, 'password' => $password));
	// 	$out = array('error' => false);
	// 	if ($q->num_rows() > 0) {
	// 		# code...
	// 		$qs = $q->row();
	// 		$out['username'] = $qs->username;
	// 		$out['status'] = 'login_sukses';
	// 	}else{
	// 		$out['status'] = 'login_gagal';
	// 	}
	// 	echo json_encode($out);
	// }
}
?>