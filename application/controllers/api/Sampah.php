<?php
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Sampah extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('status') != 'login')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
			}
	function index(){
			$data['title'] = "Jenis Sampah";
			$data['menu'] = $this->Login_m->menu();
			$sampah['data'] = $this->db->get('tbl_jenis')->result();
			$data['content'] = $this->load->view('dashboard/jenis_sampah',$sampah,true);
			$this->load->view('dashboard/index',$data);
		}
	function tambah_sampah(){
		$jenis = $this->input->post('jenis');
		$harga = $this->input->post('harga');
		$satuan = $this->input->post('satuan');

		$data = array(
						'jenis_sampah' => $jenis,
						'harga_sampah' => $harga,
						'satuan' => $satuan
					 );
		$this->db->insert('tbl_jenis', $data);
		$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Jenis Sampah Berhasil di Tambahkan", "success");
      		</script>');
		redirect('sampah');
	}
	function update(){
		$jenis = $this->input->post('jenis');
		$harga = $this->input->post('harga');
		$satuan = $this->input->post('satuan');

		$data = array(
						'jenis_sampah' => $jenis,
						'harga_sampah' => $harga,
						'satuan' => $satuan
					 );
		$this->db->update('tbl_jenis', $data, array('id' => $this->input->post('id')));
		$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Jenis Sampah Berhasil di Perbarui", "success");
      		</script>');
		redirect('sampah');
	}
    function export(){
    // Load plugin PHPExcel nya
    include APPPATH.'third_party/PHPExcel/PHPExcel.php';
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Bank Sampah')
                 ->setLastModifiedBy('Bank Sampah')
                 ->setTitle("Data Jenis Sampah")
                 ->setSubject("Jenis Sampah")
                 ->setDescription("Laporan Semua Data Jenis Sampah")
                 ->setKeywords("Data Jenis Sampah");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA JENIS SAMPAH"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:D1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "JENIS SAMPAH"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "SATUAN BERAT"); // Set kolom C3 dengan tulisan "NAMA"
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "HARGA SATUAN"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    
    $setor = $this->db->get('tbl_jenis')->result();
    $no = 1; 
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($setor as $data){ 
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->jenis_sampah);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->satuan);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, number_format($data->harga_sampah,2,',','.'));
      
      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(50); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); // Set width kolom D
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Jenis Sampah");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Jenis Sampah.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
  }
}
?>