<?php
class Dashboard extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('status') != 'login')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
			}
	function index(){
			$data['title'] = "Dashboard";
			$data['menu'] = $this->Login_m->menu();
			$dash['nasabah'] = $this->db->get_where('tbl_user', array('level' => 2))->num_rows();
			$dash['pegawai'] = $this->db->get_where('tbl_user', array('level' => 1))->num_rows();
			$dash['data'] = $this->db->query('SELECT tbl_user.*, tbl_setor.* from tbl_user INNER JOIN tbl_setor ON tbl_user.username = tbl_setor.username where tbl_user.level = 2 ORDER BY tbl_setor.id DESC')->result();			
			$dash['data2'] = $this->db->query('SELECT tbl_user.*, tbl_transaksi.*, tbl_saldo.* from tbl_user INNER JOIN tbl_transaksi ON tbl_user.username = tbl_transaksi.username INNER JOIN tbl_saldo ON tbl_user.username = tbl_saldo.username where tbl_user.level = 2 ORDER BY tbl_transaksi.id DESC')->result();			
			$data['content'] = $this->load->view('dashboard/content',$dash,true);
			$this->load->view('dashboard/index',$data);
		}
}
?>