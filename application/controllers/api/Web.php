<?php
class Web extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('status') != 'login')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
			}
	function index(){
			$data['title'] = "Content Web";
			$data['menu'] = $this->Login_m->menu();
			$web['data'] = $this->db->get('tbl_content')->result();
			$data['content'] = $this->load->view('dashboard/web',$web,true);
			$this->load->view('dashboard/index',$data);
		}
	function update(){
		$col_judul = $this->input->post('col_judul');
		$col_video = $this->input->post('col_video');
		$col_content = $this->input->post('col_content');
		$col_jam_mulai = $this->input->post('mulai');
		$col_jam_selesai = $this->input->post('selesai');
		$col_about = $this->input->post('col_about');
		$col_contact = $this->input->post('col_contact');
		$col_footer_content = $this->input->post('col_footer_content');

		$data = array(
						'col_judul' => $col_judul,
						'col_video' => $col_video,
						'col_content' => $col_content,
						'col_jam_mulai' => $col_jam_mulai,
						'col_jam_selesai' => $col_jam_selesai,
						'col_about' => $col_about,
						'col_contact' => $col_contact,
						'col_footer_content' => $col_footer_content
					 );
		$this->db->update('tbl_content', $data, array('id' => 1));
		$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Konten Web Berhasil di ubah", "success");
      		</script>');
			redirect('web');
	}
}
?>