<?php
class Profile extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('status') != 'login')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
			}
	function index(){
			$data['title'] = "Nasabah Tarik";
			$data['menu'] = $this->Login_m->menu();
			$username = $this->session->userdata('username'); 
			$tarik['data'] = $this->db->query('SELECT * FROM tbl_user WHERE username = "'.$username.'"')->result();
			$data['content'] = $this->load->view('dashboard/profile',$tarik,true);
			$this->load->view('dashboard/index',$data);
		}
	function update(){
		$username = $this->input->post('username');
		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');
		$hp = $this->input->post('hp');
		$telp = $this->input->post('telp');
		$alamat = $this->input->post('alamat');
		$password = $this->input->post('password');

		$where = array('username' => $username,);
		if (!empty($password)) {
			# code...
			$data = array(
							'fullname' => $fullname,
							'email' => $email,
							'hp' => $hp,
							'telp' => $telp,
							'alamat' => $alamat,
							'password' => $password
						 );
			$this->db->update('tbl_user', $data, $where);
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Data Pribadi berhasil di ubah", "success");
      		</script>');
			redirect('profile');
		}else{
			$data = array(
							'fullname' => $fullname,
							'email' => $email,
							'hp' => $hp,
							'telp' => $telp,
							'alamat' => $alamat
						 );
			$this->db->update('tbl_user', $data, $where);
			$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Data Pribadi berhasil di ubah", "success");
      		</script>');
			redirect('profile');
		}
	}
}
?>