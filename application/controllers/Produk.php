<?php 
class Produk extends CI_Controller
{
    function __construct()
	{
				parent::__construct();
				if($this->session->userdata('login') != 'login_admin')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
    }
    function index(){
        $data['title'] = "Toko Bangunan";
        $data['menu'] = $this->Login_m->menu();
        $content['kategori'] = $this->db->get('tbl_kategori')->result();
        $content['produk'] = $this->db->query('SELECT tbl_product_list.*, tbl_kategori.kategori_barang FROM tbl_product_list INNER JOIN tbl_kategori ON tbl_product_list.kategori = tbl_kategori.id_kategori ORDER BY tbl_product_list.kode_barang')->result();
		$data['content'] = $this->load->view('dashboard/list_produk',$content,true);
		$this->load->view('dashboard/index',$data);
    }
    function tambah(){
        $kategori = $this->input->post('kategori');
        $nama = $this->input->post('nama'); 
        $jumlah_barang = $this->input->post('jumlah');
        $harga = $this->input->post('harga');
        $satuan = $this->input->post('satuan');
        
        $q = $this->db->get_where('tbl_kategori', array('id_kategori' => $kategori));
        $create = $q->row()->kategori_barang;
        $kode = substr($create, 0, 3);
        // echo $kategori;
        $jumlah = $this->db->get_where('tbl_product_list', array('kategori' => $kategori))->num_rows();
        $jml = $jumlah + 1;
        $kode_barang = $kode."-".$jml;
        $config['upload_path']          = './assets/image_produk/';
        $config['allowed_types']        = 'jpg|jpeg';
        $config['max_size']             = 2048;
        $config['file_name']            = md5($kode_barang);

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar'))
        {
            $nama_file = $config['file_name'].$this->upload->data('file_ext');
            $data = array(
                'kategori' => $kategori,
                'kode_barang' => $kode_barang,
                'nama_barang' => $nama,
                'harga_barang' => $harga,
                'satuan_berat' => $satuan,
                'jumlah_barang' => $jumlah_barang,
                'gambar_barang' => $nama_file,
                'diskon' => 0
            );
            $this->db->insert('tbl_product_list', $data);
            $this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Sukses!", "Tambah produk Berhasil", "success");
              </script>');
            redirect('produk');
        }else{
            $this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Gagal!", "Tambah produk gagal", "error");
      		</script>');
            redirect('produk');
        }
    }
    function update(){
        $cek = $this->input->post('gambar');
        $kategori = $this->input->post('kategori');
        $nama = $this->input->post('nama'); 
        $jumlah_barang = $this->input->post('jumlah');
        $harga = $this->input->post('harga');
        $satuan = $this->input->post('satuan');
        $kode = $this->input->post('kode');
        if (empty($cek)) {
            # code...
            $data = array(
                'kategori' => $kategori,
                'nama_barang' => $nama,
                'harga_barang' => $harga,
                'satuan_berat' => $satuan,
                'jumlah_barang' => $jumlah_barang,
                'diskon' => 0
            );
            $this->db->update('tbl_product_list', $data, array('kode_barang' => $kode));
            $this->session->set_userdata('notif', '<script type="text/javascript">
            swal("Sukses!", "Edit produk Berhasil", "success");
              </script>');
            redirect('produk');
        }else{
            $config['upload_path']          = './assets/image_produk/';
            $config['allowed_types']        = 'jpg|jpeg';
            $config['max_size']             = 2048;
            $config['file_name']            = md5($kode);
            $get_img = $this->db->get_where('tbl_product_list', array('kode_barang' => $kode))->row()->gambar_barang;
            unlink(FCPATH.'/assets/image_produk/'.$get_img);

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('gambar'))
            {
                $nama_file = $config['file_name'].$this->upload->data('file_ext');
                $data = array(
                    'kategori' => $kategori,
                    'nama_barang' => $nama,
                    'harga_barang' => $harga,
                    'satuan_berat' => $satuan,
                    'jumlah_barang' => $jumlah_barang,
                    'gambar_barang' => $nama_file,
                    'diskon' => 0
                );
                $this->db->update('tbl_product_list', $data, array('kode_barang' => $kode));
                $this->session->set_userdata('notif', '<script type="text/javascript">
                swal("Sukses!", "Edit produk Berhasil", "success");
                  </script>');
                redirect('produk');
            }else{
                $this->session->set_userdata('notif', '<script type="text/javascript">
                swal("Gagal!", "Edit produk gagal", "error");
                </script>');
                redirect('produk');
            }
        }
    }
    function delete($id=''){
        if ($id == '') {
            # code...
            redirect('produk');
        }else{
            $get_img = $this->db->get_where('tbl_product_list', array('id' => $id))->row()->gambar_barang;
            unlink(FCPATH.'/assets/image_produk/'.$get_img);
            $this->db->delete('tbl_product_list', array('id' => $id));
            $this->session->set_userdata('notif', '<script type="text/javascript">
                swal("Sukses!", "Hapus produk Berhasil", "success");
                  </script>');
                redirect('produk');
        }
    }
}

?>