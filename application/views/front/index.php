<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?></title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/style.css" type="text/css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        body {font-family: Arial, Helvetica, sans-serif;}

        /* The Modal (background) */
        .modal {
          display: none; /* Hidden by default */
          position: fixed; /* Stay in place */
          z-index: 1; /* Sit on top */
          padding-top: 100px; /* Location of the box */
          left: 0;
          top: 0;
          width: 100%; /* Full width */
          height: 100%; /* Full height */
          overflow: auto; /* Enable scroll if needed */
          background-color: rgb(0,0,0); /* Fallback color */
          background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
          background-color: #fefefe;
          margin: auto;
          padding: 20px;
          border: 1px solid #888;
          width: 80%;
        }

        /* The Close Button */
        .close {
          color: #aaaaaa;
          float: right;
          font-size: 28px;
          font-weight: bold;
        }

        .close:hover,
        .close:focus {
          color: #000;
          text-decoration: none;
          cursor: pointer;
        }
    </style>
</head>

<body>
    <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
          function rupiah($angka){
            $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
            return $hasil_rupiah;
           
          }
        ?>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="#"><img src="img/logo.png" alt=""></a>
        </div>
        <div class="humberger__menu__cart">
            <ul>
                <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
            </ul>
        </div>
        <div class="humberger__menu__widget">
            <div class="header__top__right__language">
                <img src="<?php echo base_url();?>assets/front/img/language.png" alt="">
                <div>Indonesia</div>
                <span class="arrow_carrot-down"></span>
                <ul>
                    <li><a href="#">Spanis</a></li>
                    <li><a href="#">Indonesia</a></li>
                </ul>
            </div>
            <div class="header__top__right__auth">
                <a href="#"><i class="fa fa-user"></i> Login</a>
            </div>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li class="active"><a href="./index.html">Home</a></li>
                <li><a href="./shop-grid.html">Belanja</a></li>
                <li><a href="#">Halaman</a>
                    <ul class="header__menu__dropdown">
                        <li><a href="./shop-details.html">Detail Belanja</a></li>
                        <li><a href="./shoping-cart.html">Keranjang Belanja</a></li>
                    </ul>
                </li>
                <li><a href="./blog.html">Blog</a></li>
                <li><a href="./contact.html">Contact</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
        </div>
        <div class="humberger__menu__contact">
            <ul>
                <li><i class="fa fa-envelope"></i> hello@colorlib.com</li>
                <li>Free Shipping for all Order of $99</li>
            </ul>
        </div>
    </div>
    <!-- Humberger End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__left">
                            <!-- <ul>
                                <li><i class="fa fa-envelope"></i> hello@colorlib.com</li>
                            </ul> -->
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                            <div class="header__top__right__language">
                                <img src="img/language.png" alt="">
                                <div>Indonesia</div>
                                <span class="arrow_carrot-down"></span>
                                <ul>
                                    <li><a href="#">Indonesia</a></li>
                                    <li><a href="#">English</a></li>
                                </ul>
                            </div>
                            <div class="header__top__right__language">
                                <?php 
                                    if (!empty($this->session->userdata('nama'))) {
                                        # code...
                                        echo "<div>".$this->session->userdata('nama')."</div>
                                                <span class='arrow_carrot-down'></span>
                                                <ul>
                                                    <li><a href='#'>Profile</a></li>
                                                    <li><a href='".base_url('home/logout')."'>Logout</a></li>
                                                </ul>";
                                    }else{
                                        echo "<a href='#' id='myBtn'><i class='fa fa-user'></i> Login</a>";
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">

          <!-- Modal content -->
          <div class="modal-content" style="background-image: url(<?php echo base_url('assets/front/img/banner/ol.jpg');?>);">
            <span class="close">&times;</span>
            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <center><p style="font-size: 24px; margin-top: 15px;"><b>Masuk untuk belanja</b></p></center>
                    <form action="<?php echo base_url('home/login');?>" method="post">
                    <table width="100%">
                        <tr> 
                            <td>No. HP</td>
                            <td>:</td>
                            <td><input class="form-control" type="text" name="hp" placeholder="Masukan Nomor HP"></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>:</td>
                            <td><input class="form-control" type="password" name="password" placeholder="Masukan Password"></td>
                        </tr>
                        <tr>
                            <td colspan="3"><br><center><input type="submit" class="btn btn-primary" value="Masuk"></center></td>
                        </tr>
                    </table> 
                    </form>    
                </div>
                <div class="col-sm-8" style="background-color: #F0F8FF;">
                    <center><p style="font-size: 24px; margin-top: 15px;"><b>Daftar</b></p></center>
                    <form action="<?php echo base_url('home/register');?>" method="post">
                    <table width="100%">
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>:</td>
                            <td width="300px"><input class="form-control" type="text" name="fullname" placeholder="Nama Lengkap"></td>
                            <td><p style="margin-left: 15px;">Nama Panggilan</p></td>
                            <td>:</td>
                            <td><input class="form-control" type="text" name="name" placeholder="Nama Panggilan"></td>
                        </tr>
                        <tr>
                            <td>No. HP</td>
                            <td>:</td>
                            <td><input class="form-control" type="text" name="hp" placeholder="Nomor HP"></td>
                            <td><p style="margin-left: 15px;">No. Telp</p></td>
                            <td>:</td>
                            <td><input class="form-control" type="text" name="telp" placeholder="Nomor Telpon"></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td colspan="4"><textarea class="form-control" name="alamat"></textarea></td>
                        </tr>
                        <tr>
                            <td>RT / RW</td>
                            <td>:</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="rt" placeholder="RT">
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="rw" placeholder="RW">
                                    </div>
                                </div>
                            </td>
                            <td><p style="margin-left: 15px;">Password</p></td>
                            <td>:</td>
                            <td><input class="form-control" type="password" name="password" placeholder="Password"></td>
                        </tr>
                        <tr>
                            <td colspan="6"><br><center><input type="submit" class="btn btn-success" value="Daftar"></center></td>
                        </tr>
                    </table> 
                    </form>
                    <br>
                </div>
            </div>
          </div>

        </div>
        <script>
            // Get the modal
            var modal = document.getElementById("myModal");

            // Get the button that opens the modal
            var btn = document.getElementById("myBtn");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks the button, open the modal 
            btn.onclick = function() {
              modal.style.display = "block";
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
              modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
              if (event.target == modal) {
                modal.style.display = "none";
              }
            }
        </script>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="#"><p style="font-size: 32px;"><b><?php echo $content->nama_toko; ?></b></p></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="./index.html">Home</a></li>
                            <li><a href="./shop-grid.html">Lihat Keranjang</a></li>
                            <!-- <li><a href="#">Belanja</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="./shop-details.html">Detail Belanja</a></li>
                                    <li><a href="./shoping-cart.html">Keranjang Belanja</a></li>
                                </ul>
                            </li> -->
                            <!-- <li><a href="./blog.html">Blog</a></li>
                            <li><a href="./contact.html">Contact</a></li> -->
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <!-- <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li> -->
                            <?php if(!empty($this->session->userdata('nama'))){ ?>
                                <li><a href="#"><i class="fa fa-shopping-bag"></i> <span><?php echo $cart; ?></span></a></li>
                            <?php } ?>
                        </ul>
                        <!-- <div class="header__cart__price">item: <span>$150.00</span></div> -->
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->

    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Kategori</span>
                        </div>
                        <ul>
                            <?php foreach($kategori as $k){?>
                                <li><a href="<?php echo base_url('home/category/'.$k->id)?>"><?php echo $k->kategori_barang; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="#">
                                <div class="hero__search__categories">
                                    Semua Kategori
                                    <span class="arrow_carrot-down"></span>
                                </div>
                                <input type="text" placeholder="Masukan Nama Barang">
                                <button type="submit" class="site-btn">Cari</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5><?php echo $content->nomer_hp; ?></h5>
                                <span>support 24/7</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg" data-setbg="<?php echo base_url();?>assets/front/img/banner/back.jpg">
                        <div class="hero__text">
                            <h2>Toko Bangunan <br /><?php echo $content->nama_toko; ?></h2>
                            <p><?php echo $content->visi; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

   <?php echo $isi; ?>
    <br><br>
    <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="#"><p style="font-size: 32px;"><b><?php echo $content->nama_toko; ?></b></p></a>
                        </div>
                        <ul>
                            <li>Alamat: <?php echo $content->alamat;?></li>
                            <li>Nomor Hp: <?php echo $content->nomer_hp;?></li>
                            <li>Email: <?php echo $content->email;?></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <!-- <h6>Useful Links</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">About Our Shop</a></li>
                            <li><a href="#">Secure Shopping</a></li>
                            <li><a href="#">Delivery infomation</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Innovation</a></li>
                            <li><a href="#">Testimonials</a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved <i class="fa fa-heart" aria-hidden="true"></i> by <a href="#" target="_blank">tbku.com</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                        <!-- <div class="footer__copyright__payment"><img src="<?php echo base_url();?>assets/front/img/payment-item.png" alt=""></div> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="<?php echo base_url();?>assets/front/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/front/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/front/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url();?>assets/front/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>assets/front/js/jquery.slicknav.js"></script>
    <script src="<?php echo base_url();?>assets/front/js/mixitup.min.js"></script>
    <script src="<?php echo base_url();?>assets/front/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/front/js/main.js"></script>



</body>

</html>