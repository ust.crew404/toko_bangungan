<div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
          function rupiah($angka){
	
            $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
            return $hasil_rupiah;
         
        }
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Produk</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Produk</h6>
            </div>
            <div class="card-body">
                <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Tambahkan Produk</span>
                          </a> 
              <hr>
              <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                    
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                        <center><p><b>Tambah Produk</b></p></center>
                        <!-- <br> -->
                        <form action="<?php echo base_url('produk/tambah/');?>" method="post" enctype="multipart/form-data">
                        <table width="100%">
                            <tr>
                                <td style="padding:5px;">Kode Barang</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" type="text" name="kode" value="-" readonly=""/></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Nama Barang</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" placeholder="Nama Barang" type="text" name="nama" /></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Kategori Barang</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                <select class="form-control" name="kategori">
                                    <option disabled="" selected="">-- Pilih Kategori --</option>
                                    <?php foreach($kategori as $k){
                                            echo '<option value="'.$k->id_kategori.'">'.$k->kategori_barang.'</option>';
                                        }?>
                                </select></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Jumlah Barang</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" step="any" type="number" name="jumlah" min="0" value="0" /></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Harga Barang</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" type="number" name="harga" min="0" value="0" /></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Satuan Barang</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" placeholder="Kg/ltr/pcs" type="text" name="satuan" /></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Gambar Barang</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" type="file" name="gambar" accept=".jpg,.jpeg" /></td>
                            </tr>
                            <tr>
                                <td colspan="3"  style="padding:5px;"><center><input class="btn btn-primary" type="submit" value="Tambah"/></center></td>
                            </tr>
                        </table>
                        </form>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                    </div>
                </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Kategori</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Barang</th>
                    <th>Gambar Barang</th>
                    <th></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  <th>No.</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Kategori</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Barang</th>
                    <th>Gambar Barang</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                    <?php $t = 1; $m= 1; $no=1; foreach ($produk as $p) { ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $p->kode_barang ?></td>
                        <td><?php echo $p->nama_barang ?></td>
                        <td><?php echo $p->kategori_barang ?></td>
                        <td><?php echo $p->jumlah_barang." ".$p->satuan_berat; ?></td>
                        <td><?php echo rupiah($p->harga_barang)." / ".$p->satuan_berat; ?></td>
                        <td><center><img src="<?php echo base_url('assets/image_produk/'.$p->gambar_barang) ?>" width="50px" height="50px"/></center></td>
                        <td><a href="#" title="Edit" data-toggle="modal" data-target="#modal_edit<?php echo $t++ ?>" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                          </a>
                          <!-- Modal -->
                            <div class="modal fade" id="modal_edit<?php echo $m++ ?>" role="dialog">
                                <div class="modal-dialog">
                                
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                    <center><p><b>Edit Produk</b></p></center>
                                    <!-- <br> -->
                                    <form action="<?php echo base_url('produk/update/');?>" method="post" enctype="multipart/form-data">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td style="padding:5px;">Kode Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;"><input class="form-control" type="text" name="kode" value="<?php echo $p->kode_barang ?>" readonly=""/></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Nama Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;"><input class="form-control" value="<?php echo $p->nama_barang ?>" type="text" name="nama" /></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Kategori Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;">
                                            <select class="form-control" name="kategori">
                                                <option disabled="" selected="">-- Pilih Kategori --</option>
                                                <option value="<?php echo $p->kategori ?>" selected=""><?php echo $p->kategori_barang ?></option>
                                                <?php foreach($kategori as $k){
                                                        echo '<option value="'.$k->id.'">'.$k->kategori_barang.'</option>';
                                                    }?>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Jumlah Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;"><input class="form-control" type="number" value="<?php echo $p->jumlah_barang ?>" name="jumlah" min="0" value="0" step="any" /></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Harga Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;"><input class="form-control" value="<?php echo $p->harga_barang ?>" type="number" name="harga" min="0" value="0" /></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Satuan Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;"><input class="form-control" value="<?php echo $p->satuan_berat ?>" placeholder="Kg/ltr/pcs" type="text" name="satuan" /></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Gambar Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;"><input class="form-control" type="file" name="gambar" accept=".jpg,.jpeg" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><center><img src="<?php echo base_url('assets/image_produk/'.$p->gambar_barang) ?>" width="100px" height="100px"/></center></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"  style="padding:5px;"><center><input class="btn btn-primary" type="submit" value="Edit"/></center></td>
                                        </tr>
                                    </table>
                                    </form>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                
                                </div>
                            </div>
                          &nbsp;<a onclick="return confirm('Yakin ingin menghapus produk?')" href="<?php echo base_url('produk/delete/'.$p->id);?>" title="Hapus" class="btn btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-trash"></i>
                            </span>
                          </a> </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid
