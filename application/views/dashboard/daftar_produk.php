<div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
          function rupiah($angka){
	
            $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
            return $hasil_rupiah;
         
        }
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Produk</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Produk</h6>
            </div>
            <div class="card-body">
                <table width="100%">
                    <tr>
                        <th>No. </th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Harga Satuan</th>
                        <th>Qty</th>
                        <th>Sub Total</th>
                        <th></th>
                    </tr>
                    <?php $n=1; $sub_total = 0; $total = 0; $data_id = ''; $data_kode = ''; $data_nama = ''; $data_harga = ''; $data_qty = ''; $data_total = '';
                    foreach ($bill as $b) { ?>
                      <tr>
                        <td><?php echo $n++ ?></td>
                        <td><?php echo $b->kode_barang ?></td>
                        <td><?php echo $b->nama_barang ?></td>
                        <td><?php echo rupiah($b->harga_barang); ?></td>
                        <td><?php echo $b->qty." ".$b->satuan_berat; ?></td>
                        <td><?php $sub_total = $b->qty * $b->harga_barang; $total += $sub_total;
                            echo rupiah($sub_total); ?></td>
                        <td><a href="<?php echo base_url('pembelian/hapus_bill/'.$b->id_cart);?>" title="Hapus" class="btn btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-trash"></i>
                            </span>
                          </a></td>
                        </tr>
                    <?php
                        $data_id .= $b->id_cart.' || ';
                        $data_kode .= $b->kode_barang.' || ';
                        $data_nama .= $b->nama_barang.' || ';
                        $data_harga .= rupiah($b->harga_barang).' || ';
                        $data_qty .= $b->qty.' / '.$b->satuan_berat.' || ';
                        $data_total .= $sub_total.' || ';
                    }?>
                    <tr>
                        <td colspan="5"><hr><b>Total Tagihan</b></td>
                        <td><hr><b><?php echo rupiah($total); ?></b></td>
                    </tr>
                </table>
                <br>
                
                <p align="right"><a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Lanjut Pembayaran</span>
                          </a> </p>
              <hr>
              <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                    
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                        <center><p><b>Detail Pembeli</b></p></center>
                        <!-- <br> -->
                        <form action="<?php echo base_url('pembelian/pembayaran/');?>" method="post" enctype="multipart/form-data">
                        <table width="100%">
                            <tr>
                                <td style="padding:5px;">Total Tagihan</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                <input class="form-control" type="text" name="total1" value="<?php echo rupiah($total);?>" readonly=""/>
                                <input class="form-control" type="text" name="data_id" value='<?php echo $data_id;?>' hidden=""/>
                                <input class="form-control" type="text" name="total" value="<?php echo $total; ?>" hidden="" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Nama Pembeli</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" placeholder="Nama Pembeli" type="text" name="nama" /></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Nomor HP</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input placeholder="Nomor HP (Optional)" class="form-control" type="text" name="hp"/></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Alamat Pengiriman</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><textarea placeholder="Alamat (Optional)" class="form-control" name="alamat"></textarea></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Status Pembayaran</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                <!-- <button onclick="functionlunas()">Lunas</button>
                                <button onclick="functionkredit()">Kredit</button> -->
                                <select id="status" class="form-control" name="kategori">
                                    <option disabled="" selected="">-- Pilih Status --</option>
                                    <option value="1">Lunas</option>
                                    <option value="0">Kredit</option>
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Terbayar</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                    <input id="kredit" class="form-control" type="number" name="harga" min="0" value="0"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;"><p id="p"></p></td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                    <input id="kembalian" class="form-control" type="text" name="kembalian" min="0" value="0" hidden="" readonly="" />
                                    <input id="piutang" class="form-control" type="text" name="piutang" min="0" value="0" hidden="" readonly="" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"  style="padding:5px;"><center><input class="btn btn-primary" type="submit" value="Bayar"/></center></td>
                            </tr>
                        </table>
                        </form>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                    </div>
                </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Stok</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Jumlah Pesan</th>
                    <th></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Stok</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Jumlah Pesan</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                    <?php $no=1; foreach ($produk as $p) { ?>
                    
                    <tr>
                        <form action="<?php echo base_url('pembelian/add'); ?>" method="post">
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $p->kode_barang ?></td>
                        <td><?php echo $p->nama_barang ?></td>
                        <td><?php echo $p->kategori_barang ?></td>
                        <td><?php echo $p->jumlah_barang." ".$p->satuan_berat; ?></td>
                        <td><?php echo rupiah($p->harga_barang)." / ".$p->satuan_berat; ?></td>
                        <td><center><img src="<?php echo base_url('assets/image_produk/'.$p->gambar_barang) ?>" width="50px" height="50px"/></center></td>
                        <td><input type="number" hidden="" value="<?php echo $p->id ?>" class="form-control" name="id_produk" />
                        <input type="number" min="0" step="any" placeholder="0" class="form-control" name="qty" required="TRUE" /></td>
                        <td><input type="submit" class="btn btn-success" value="Tambahkan"></td>
                        </form>
                    </tr>
                    
                    <?php } ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#status').change(function(){
                    var status = $(this).val();
                    if (status == 0) {
                        document.getElementById('kembalian').hidden = true;
                        document.getElementById('piutang').hidden = false;
                        document.getElementById("p").innerHTML = "Sisa Piutang";
                        $('#kredit').change(function(){
                            var piutang = $(this).val();
                            var total = <?php echo $total; ?>;
                            var sisa_piutang = total - piutang;
                            document.getElementById("piutang").value = formatRupiah(sisa_piutang, 'Rp. ');
                        }); 
                    }else{
                        document.getElementById('kembalian').hidden = false;
                        document.getElementById('piutang').hidden = true;
                        document.getElementById("p").innerHTML = "Kembalian";
                        $('#kredit').change(function(){
                            var kredit = $(this).val();
                            var tagihan = <?php echo $total; ?>;
                            var kembalian = kredit - tagihan;
                            document.getElementById("kembalian").value = formatRupiah(kembalian, 'Rp. ');
                        });                        
                    }
                });
            });
            function formatRupiah(angka, prefix){
                var number_string = angka.toString(),
                split           = number_string.split(','),
                sisa            = split[0].length % 3,
                rupiah          = split[0].substr(0, sisa),
                ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
     
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
     
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }
        </script>
