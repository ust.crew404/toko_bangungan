-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2020 at 10:19 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_bangunan`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_cart`
--

CREATE TABLE `detail_cart` (
  `id` int(11) NOT NULL,
  `nama_pembeli` varchar(50) NOT NULL,
  `hp` varchar(25) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `status_pembayaran` int(1) NOT NULL,
  `terbayar` int(50) NOT NULL,
  `sisa_piutang` int(50) DEFAULT NULL,
  `produk_dibeli` text NOT NULL,
  `total` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_cart`
--

INSERT INTO `detail_cart` (`id`, `nama_pembeli`, `hp`, `alamat`, `status_pembayaran`, `terbayar`, `sisa_piutang`, `produk_dibeli`, `total`) VALUES
(3, 'Nur Bekti Setiyanto', '082133619739', 'Pakuncen', 0, 15000000, 5045000, '6 || 7 || ', 20045000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `id_cart` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `qty` float NOT NULL,
  `is_bayar` int(1) NOT NULL,
  `log_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id_cart`, `id_user`, `id_product`, `qty`, `is_bayar`, `log_date`) VALUES
(6, 1, 2, 3, 1, '2020-07-05 15:13:22'),
(7, 1, 3, 2, 1, '2020-07-05 15:13:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE `tbl_customers` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `nama_panggilan` varchar(25) NOT NULL,
  `nomor_hp` varchar(15) NOT NULL,
  `nomor_telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `rt` varchar(5) NOT NULL,
  `rw` varchar(5) NOT NULL,
  `password` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`id`, `nama_lengkap`, `nama_panggilan`, `nomor_hp`, `nomor_telp`, `alamat`, `rt`, `rw`, `password`, `create_date`) VALUES
(1, 'Muhamad Aziz Setiyalaksono', 'aziz', '082133619739', '081327213502', 'Desa Pakuncen', '04', '01', '200820e3227815ed1756a6b531e7e0d2', '2020-06-30 07:44:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id_kategori` int(11) NOT NULL,
  `kategori_barang` varchar(50) NOT NULL,
  `log_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `kategori_barang`, `log_date`) VALUES
(1, 'Baja', '2020-06-30 05:04:52'),
(2, 'Batu', '2020-06-30 05:05:00'),
(3, 'Kayu', '2020-06-30 05:05:10'),
(4, 'Semen', '2020-06-30 05:05:28'),
(5, 'Beton', '2020-06-30 05:06:28'),
(6, 'Logam', '2020-06-30 05:06:39'),
(7, 'Pasir', '2020-06-30 05:06:50'),
(8, 'Paku', '2020-06-30 05:07:39'),
(9, 'Besi', '2020-06-30 05:08:10'),
(10, 'Pengikat', '2020-06-30 05:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(50) NOT NULL,
  `parent` int(2) NOT NULL,
  `url` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `user_level` int(1) NOT NULL,
  `set_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `menu`, `parent`, `url`, `icon`, `user_level`, `set_active`) VALUES
(1, 'Dashboard', 0, 'dashboard', 'fas fa-fw fa-tachometer-alt', 1, 1),
(2, 'Customers', 0, '#', 'fas fa-fw fa-folder', 1, 1),
(3, 'History Customers', 2, 'Customers', '', 1, 1),
(26, 'Produk', 0, '#', 'fas fa-fw fa-folder', 1, 1),
(27, 'Daftar Produk', 26, 'produk', '', 1, 1),
(28, 'Pembelian', 0, '#', 'fas fa-fw fa-folder', 1, 1),
(29, 'Input Pembelian', 28, 'pembelian', '', 1, 1),
(30, 'Piutang', 0, '#', 'fas fa-fw fa-folder', 1, 1),
(31, 'Daftar Piutang', 30, 'piutang', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_list`
--

CREATE TABLE `tbl_product_list` (
  `id` int(11) NOT NULL,
  `kategori` int(5) NOT NULL,
  `kode_barang` varchar(10) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `satuan_berat` varchar(15) NOT NULL,
  `jumlah_barang` float NOT NULL,
  `gambar_barang` varchar(100) NOT NULL,
  `diskon` int(5) NOT NULL,
  `logdate` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product_list`
--

INSERT INTO `tbl_product_list` (`id`, `kategori`, `kode_barang`, `nama_barang`, `harga_barang`, `satuan_berat`, `jumlah_barang`, `gambar_barang`, `diskon`, `logdate`) VALUES
(2, 8, 'Pak-1', 'Paku Beton', 15000, 'Kg', 8, '6e322458f7b82ac521456ca2e7e2da8b.jpg', 0, '2020-07-05 02:00:35'),
(3, 3, 'Kay-1', 'Kayu Jati', 10000000, 'pcs', 46, '05a7ad778b38f67d95d1dd749c2b49c2.jpg', 0, '2020-07-05 02:00:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `fullname`, `password`, `level`) VALUES
(1, 'demo', 'Fikriyudin', '62cc2d8b4bf2d8728120d052163a77df', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_website`
--

CREATE TABLE `tbl_website` (
  `id` int(11) NOT NULL,
  `nama_toko` varchar(100) NOT NULL,
  `visi` text NOT NULL,
  `alamat` text NOT NULL,
  `nomer_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_website`
--

INSERT INTO `tbl_website` (`id`, `nama_toko`, `visi`, `alamat`, `nomer_hp`, `email`) VALUES
(1, 'Berkah Mulia', 'Anda Puas Kami Senang.', 'Desa Pakuncen, RT 04/01 Kecamatan. Bobotsari, Kabupaten Purbalingga', '082133619739', 'ust.crew404@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_cart`
--
ALTER TABLE `detail_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indexes for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product_list`
--
ALTER TABLE `tbl_product_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_website`
--
ALTER TABLE `tbl_website`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_cart`
--
ALTER TABLE `detail_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `id_cart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_product_list`
--
ALTER TABLE `tbl_product_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_website`
--
ALTER TABLE `tbl_website`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
